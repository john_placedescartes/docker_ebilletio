## Virtualisation (windows)


Par défaut, le service Hyper-V n’est pas activé sur Windows.   
Pour vérifier s'il est activé, on va dans ``` Gestionnaire des tâches```  et dans l'onglet ``` Performance```  voir si la virtualisation n'est pas activée      
   
![screen virtu windows](https://www.grafikart.fr/uploads/2015/08/hyperv-virtu.jpg)
   
Si ce n’est pas le cas, il va falloir aller dans le BIOS (f10 pour nos HP), aller dans la partie ``` avanced``` et cocher la case ``` VTx``` 

## Config:

### Dockerfile.development:

Choisir sa config php => ``` FROM webdevops/php-apache:debian-8 ``` crée le dossier app.

### docker-compose.yml:

PDC est relié au port 8002  

PDC utilise le fichier ``` etc/environment.yml ``` pour paramétrer l'environnement web ou BDD: bien pensé a modifier la variable ``` WEB_DOCUMENT_ROOT ```  si on change le nom du dossier  

Les variables ``` MYSQL_ROOT_PASSWORD ```  ``` MYSQL_USER ```  ``` MYSQL_PASSWORD ```  ``` MYSQL_DATABASE ```  permettent de paramétrer la BDD   

## Installation:

``` docker-compose up -d ``` 

Dans sourcestree (ou autre), configurer le dossier app (cloner, composer install, remplir les .env etc...)

### Connaitre l'hôte mysql

Afficher les containers (tous devraient être à up) 

``` docker ps -a ``` 

Identifier le container mysql (et son id) et lancer la requête (exec permet de lancer des commandes sur la machine): 

``` docker exec container_id mysql -u root -p'dev' -e 'select @@hostname' ``` 

Récupérer la valeur retournée et la mettre comme DB_HOST dans les .env 

Normalement tout devrait bien marcher :)

## Autres commandes utiles

### Crasher les containers

//on stop les containers   
``` docker-compose stop ``` 

//on les supprime   
``` docker-compose rm --force ``` 

//et on rebuild   
``` docker-compose build --no-cache ``` 

//et on up   
``` docker-compose up -d ```

### Faire un php artisan (par exemple)

``` docker exec -ti nom_de_l_app php artisan migrate ``` (ou rajouter winpty devant si le -ti ne fonctionne pas)

## Virtual hosts:

Rajouter 2 lignes dans le fichier hosts de windows ``` C:\Windows\System32\drivers\etc\hosts ```   
``` 127.0.0.1       pdc.dev ```    

Et vous pouvez accéder à PDC via http://pdc.dev:8002

## Astuce

Taper un container_id en entier n'est pas nécessaire, il faut simplement indiquer le début de l'id jusqu'à que ça en devienne "discriminant"  

Exemple :

| CONTAINER ID | IMAGE                 |
|--------------|-----------------------|
| 94772c74d0f1 | myapp_app             |
| 4ed3d348961a | phpmyadmin/phpmyadmin |
| 9f7ca9696620 | myapp_mysql           |   

Si on veut récupérer l'id de  ```phpmyadmin/phpmyadmin ``` seul "4" est suffisant  
Par contre si on veut récupérer celle de  ```myapp_app ``` "9" n'est pas suffisant vu que l'id de  ```myapp_mysql ``` commence aussi par "9" donc il faudra taper "94"  

Ainsi si on veut faire un "ls" sur  ```myapp_app ``` on peut faire:  
``` docker exec 94 ls ```

### Synchroniser l'heure de la machine et du container (sous windows)

Aller dans l'application ```Gestionnaire hyper-v```  
Cliquer sur ```AlesaX``` à gauche   
A droite dans le menu ```MobyLinuxVM```, cliquer sur ```paramètres``` puis ```services d'intégration``` et cocher ```synchronisation date/heure```

### Modifier le timeout PMA de 1140 sec (et installer nano sur alpine)

L'image docker PMA tourne sous alpine linux ```docker exec container_id_PMA cat etc/issue``` (pour s'en assurer)  
Etape 1: installer nano

```docker exec container_id_PMA apk add --no-cache nano```   

Etape 2: on ouvre le fichier ```config.user.inc.php``` maintenant que nano est bien installé

```winpty docker exec -it container_id_PMA nano etc/phpmyadmin/config.user.inc.php```

Etape 3: on écrit dans le fichier   
```<?php $cfg['LoginCookieValidity'] = 15000000000; ?>```

Etape 4: on stop le container et on le relance  
```docker stop container_id_PMA ```   
```docker-compose up -d ```

### Modifier le max upload des imports sql sur PMA

Par défaut, la limite d'upload des imports sql est limité à 512mo (ce qui est déjà pas mal) mais au besoin on peut modifier le fichier php.ini de PMA   
On s'arme de notre super nano est on est partis :

```winpty docker exec -it container_id nano etc/php.ini```

On va modifier le ```post_max_size``` et le ```upload_max_filesize```   
   
Les petits raccourcis nano qui vont bien:   
CTRL + W : Permet de faire une recherche   
CTRL + E : Permet d'aller à la fin de la ligne (si les flèches ne fonctionnent pas, ça sauve la vie)   
et CTRL + X : Permet de quitter nano   
   
Donc on modifie le ```post_max_size``` et le ```upload_max_filesize``` à ```1G``` par exemple   

On stop le container et on le relance  
```docker stop container_id ```   
```docker-compose up -d ```